<?php

namespace src\controllers;

use src\models\ClientModel;

class Client {

	private function getClientModel(): ClientModel {
		return new ClientModel();
	}

	public function getClients() {
		return $this->getClientModel()->getClients();
	}

	public function createClient($client) {
        // TODO: [Feature] Phone added should be valid for US (In this case we need to use an API. The details are in the repository)

        $email = $client['email'] ?? '';
        $isValidEmail = filter_var($email, FILTER_VALIDATE_EMAIL);
        if (!$isValidEmail) {
            throw new \Exception( "Email is NOT a valid email address.");
        }

        return $this->getClientModel()->createClient($client);
	}

	public function updateClient($client) {
        // TODO: [Feature] Phone added should be valid for US (In this case we need to use an API. The details are in the repository)
        // TODO: [Feature] Email added should be valid
		return $this->getClientModel()->updateClient($client);
	}

	public function getClientById($id) {
		return $this->getClientModel()->getClientById($id);
	}
}