<?php

namespace src\controllers;

use src\models\BookingModel;

class Booking {

	private function getBookingModel(): BookingModel {
		return new BookingModel();
	}

	public function getBookings() {
		return $this->getBookingModel()->getBookings();
	}

    public function createBooking($booking) {
        // TODO: implement a validation about the client and dogs existance.
        return $this->getBookingModel()->createBooking($booking);
    }
}