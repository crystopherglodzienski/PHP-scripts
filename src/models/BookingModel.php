<?php

namespace src\models;

use src\helpers\Helpers;

class BookingModel {

	private $bookingData;

	function __construct() {
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/bookings.json');
		$this->bookingData = json_decode($string, true);
	}

	public function getBookings() {
		return $this->bookingData;
	}

    public function createBooking($data) {
        $bookings = $this->getBookings();

        var_dump();

        $data['id'] = end($bookings)['id'] + 1;
        $bookings[] = $data;

        var_dump($bookings);

        Helpers::putJson($bookings, 'bookings');

        return $data;
    }
}