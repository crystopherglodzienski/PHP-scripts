<?php

namespace src\models;

use src\helpers\Helpers;

class ClientModel {

	private $clientData;

	function __construct() {
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/clients.json');
		$this->clientData = json_decode($string, true);
	}

	public function getClients() {
        // TODO: [Feature] Add possibility to soft delete a client (add tests to support it)
		return $this->clientData;
	}

	public function createClient($data) {
		$clients = $this->getClients();

        //TODO: [Feature] Restrict to unique client emails on the database (and add tests to support it)
        // TODO: [Feature] Add possibilitety to soft delete a client (add tests to support it)

		$data['id'] = end($clients)['id'] + 1;
		$clients[] = $data;

		Helpers::putJson($clients, 'clients');

		return $data;
	}

	public function updateClient($data) {
		$updateClient = [];
		$clients = $this->getClients();
		foreach ($clients as $key => $client) {
			if ($client['id'] == $data['id']) {
				$clients[$key] = $updateClient = array_merge($client, $data);
			}
		}

        Helpers::putJson($clients, 'clients');

		return $updateClient;
	}

	public function getClientById($id) {
		$clients = $this->getClients();
		foreach ($clients as $client) {
			if ($client['id'] == $id) {
				return $client;
			}
		}
		return null;
	}
}